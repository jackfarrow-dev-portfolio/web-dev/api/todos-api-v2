package com.todosapi.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class HttpResponseObject {

    private HttpStatus httpStatus;

    private int status;
    private Object data;
    private ZonedDateTime zonedDateTime;

    public HttpResponseObject(
            HttpStatus status,
            Object data
    ) {
        this.httpStatus = status;
        this.status = status.value();
        this.data = data;
        this.zonedDateTime = ZonedDateTime.now();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus status) {
        this.httpStatus = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }

    public void setZonedDateTime(ZonedDateTime zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }

    @Override
    public String toString() {
        return "HttpResponseObject{" +
                "httpStatus=" + httpStatus +
                ", status=" + status +
                ", data=" + data +
                ", zonedDateTime=" + zonedDateTime +
                '}';
    }
}
