package com.todosapi.todo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TodosRepository extends CrudRepository<Todo, UUID> {

    @Modifying
    @Query("UPDATE Todo t SET t.description = ?1, t.status = ?2, t.title = ?3, t.categoryId = ?4 WHERE t.id = ?5")
    int updateTodo(String description, TodoStatus status, String title, UUID id, UUID categoryId);

}
