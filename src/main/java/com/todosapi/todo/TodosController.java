package com.todosapi.todo;

import com.todosapi.exception.HttpResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v2/todos")
public class TodosController {

    private final TodosService todosService;

    @Autowired
    public TodosController(TodosService todosService) {
        this.todosService = todosService;
    }

    @GetMapping("get/{id}")
    public ResponseEntity<HttpResponseObject> getTodoById(@PathVariable(name = "id") String id) {
        HttpResponseObject todo = this.todosService.getTodoById(id);
        return new ResponseEntity<>(todo, todo.getHttpStatus());
    }

    @GetMapping("get")
    public ResponseEntity<HttpResponseObject> getTodos() {
        HttpResponseObject todos = this.todosService.getTodos();
        return new ResponseEntity<>(todos, todos.getHttpStatus());
    }

    @PostMapping("new")
    public ResponseEntity<HttpResponseObject> createTodo(@RequestBody Todo todo) {
        HttpResponseObject response = this.todosService.createTodo(todo);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("update")
    public ResponseEntity<HttpResponseObject> updateTodo(@RequestBody Todo todo) {
        HttpResponseObject response = this.todosService.updateTodo(todo);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<HttpResponseObject> deleteTodo(@PathVariable(name = "id") String id) {
        HttpResponseObject response = this.todosService.deleteTodo(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
