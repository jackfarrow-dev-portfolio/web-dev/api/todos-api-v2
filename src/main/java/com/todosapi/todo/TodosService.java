package com.todosapi.todo;

import com.todosapi.category.Category;
import com.todosapi.category.CategoryRepository;
import com.todosapi.exception.*;
import com.todosapi.todo.Todo;
import com.todosapi.todo.TodosRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class TodosService {

    private final TodosRepository todosRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public TodosService(TodosRepository todosRepository, CategoryRepository categoryRepository) {
        this.todosRepository = todosRepository;
        this.categoryRepository = categoryRepository;
    }

    public HttpResponseObject createTodo(Todo todo) {
        try {
            todo.setCreateDate(LocalDateTime.now());
            Todo savedTodo = this.todosRepository.save(todo);
            return new HttpResponseObject(HttpStatus.CREATED, savedTodo);
        } catch (Exception e) {
            throw new ServerException("Failed to save todo");
        }

    }

    @Transactional
    public HttpResponseObject updateTodo(Todo todo) {
        try {
            Optional<Todo> optional = this.todosRepository.findById(todo.getId());
            if (optional.isEmpty()) {
                throw new BadRequestException("Todo identified by " + todo.getId() + " not found");
            }
            int updateResult = this.todosRepository.updateTodo(
                    todo.getDescription(),
                    todo.getStatus(),
                    todo.getTitle(),
                    todo.getCategoryId(),
                    todo.getId()
            );
            if (updateResult != 1) {
                throw new ServerException("Failed to update todo identified by: " + todo.getId());
            }
            return new HttpResponseObject(HttpStatus.OK, null);
        } catch (Exception e) {
            throw new ServerException("Failed to update todo: " + todo.getId());
        }
    }

    public HttpResponseObject deleteTodo(String id) {
        UUID idAsUUID = UUID.fromString(id);
        Optional<Todo> optional = this.todosRepository.findById(idAsUUID);
        if (optional.isEmpty()) {
            throw new BadRequestException("Todo identified by " + id + " not found");
        }
        try {
            this.todosRepository.deleteById(idAsUUID);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
        return new HttpResponseObject(
                HttpStatus.OK,
                null
        );
    }

    public HttpResponseObject getTodos() {
        try {
            Iterable<Todo> todos = this.todosRepository.findAll();
            return new HttpResponseObject(HttpStatus.OK, todos);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

    public HttpResponseObject getTodoById(String id) {
        Optional<Todo> optional = null;
        try {
            UUID idAsUUID = UUID.fromString(id);
            optional = this.todosRepository.findById(idAsUUID);
            if (optional.isEmpty()) {
                throw new NotFoundException("Todo identified by id: " + id + " not found");
            }
            Todo foundTodo = optional.get();
            return new HttpResponseObject(HttpStatus.OK, foundTodo);
        } catch(IllegalArgumentException e) {
            throw new BadRequestException("Id " + id + " is invalid");
        }
    }
}
