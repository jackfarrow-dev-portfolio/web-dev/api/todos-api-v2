package com.todosapi.todo;

import com.todosapi.todo_to_tag.TodoToTag;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "todo")
public class Todo {
    @Id
    @Column(name = "todo_id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Column(name="title")
    private String title;
    @Column(name="description")
    private String description;
    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private TodoStatus status;
    @Column(name="create_date")
    private LocalDateTime createDate;
    @Column(name="complete_date")
    private LocalDateTime completeDate;
    @Column(name="last_updated_on")
    private LocalDateTime lastUpdatedOn;
    @Column(name="category_id")
    private UUID categoryId;

    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
            mappedBy = "todo"
    )
    private List<TodoToTag> todoToTags = new ArrayList<>();
    public Todo() {
    }

    public Todo(
            String title,
            String description,
            TodoStatus status,
            LocalDateTime createDate,
            UUID categoryId,
            LocalDateTime lastUpdatedOn,
            List<TodoToTag> t
    ) {
        this.title = title;
        this.description = description;
        this.status = status;
        this.createDate = createDate;
        this.categoryId = categoryId;
        this.lastUpdatedOn = lastUpdatedOn;
        this.todoToTags = t;
    }

    public Todo(
            UUID id,
            String title,
            String description,
            TodoStatus status,
            LocalDateTime createDate,
            UUID categoryId,
            LocalDateTime lastUpdatedOn
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.createDate = createDate;
        this.categoryId = categoryId;
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public Todo(
            UUID id,
            String title,
            String description,
            TodoStatus status,
            LocalDateTime createDate,
            LocalDateTime completeDate,
            UUID categoryId,
            LocalDateTime lastUpdatedOn
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.createDate = createDate;
        this.completeDate = completeDate;
        this.categoryId = categoryId;
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TodoStatus getStatus() {
        return status;
    }

    public void setStatus(TodoStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public List<TodoToTag> getTodoToTags() {
        return todoToTags;
    }

    public void setTodoToTags(List<TodoToTag> todoToTags) {
        this.todoToTags = todoToTags;
    }

    public void addTodoToTag(TodoToTag t) {
        if (!this.todoToTags.contains(t)) {
            this.todoToTags.add(t);
        }
    }

    public void removeTodoToTag(TodoToTag t) {
        this.todoToTags.remove(t);
    }
    public LocalDateTime getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(LocalDateTime completeDate) {
        this.completeDate = completeDate;
    }

    public LocalDateTime getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id) && Objects.equals(title, todo.title) && Objects.equals(description, todo.description) && status == todo.status && Objects.equals(createDate, todo.createDate) && Objects.equals(completeDate, todo.completeDate) && Objects.equals(lastUpdatedOn, todo.lastUpdatedOn) && Objects.equals(categoryId, todo.categoryId) && Objects.equals(todoToTags, todo.todoToTags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, status, createDate, completeDate, lastUpdatedOn, categoryId, todoToTags);
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", createDate=" + createDate +
                ", completeDate=" + completeDate +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", categoryId=" + categoryId +
                ", todoToTags=" + todoToTags +
                '}';
    }
}
