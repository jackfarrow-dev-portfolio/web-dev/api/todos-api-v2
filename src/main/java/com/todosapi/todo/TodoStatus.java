package com.todosapi.todo;

public enum TodoStatus {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    SUSPENDED
}
