package com.todosapi.http;

import com.todosapi.category.Category;

import java.util.Objects;

public class CategoryResponse {

    private Iterable<Category> categories;

    public CategoryResponse(Iterable<Category> categories) {
        this.categories = categories;
    }

    public Iterable<Category> getCategories() {
        return categories;
    }

    public void setCategories(Iterable<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryResponse that = (CategoryResponse) o;
        return Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categories);
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "categories=" + categories +
                '}';
    }
}
