package com.todosapi.tag;

import com.todosapi.todo.Todo;
import com.todosapi.todo_to_tag.TodoToTag;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name="tag_id")
    private UUID tagId;
    @Column(name="tag_name")
    private String tagName;

    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
            mappedBy = "tag"
    )
    private List<TodoToTag> todoToTags = new ArrayList<>();
    public Tag() {
    }

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public Tag(UUID tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    public UUID getTagId() {
        return tagId;
    }

    public void setTagId(UUID tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public List<TodoToTag> getTodoToTags() {
        return todoToTags;
    }

    public void setTodoToTags(List<TodoToTag> todoToTags) {
        this.todoToTags = todoToTags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(tagId, tag.tagId) && Objects.equals(tagName, tag.tagName) && Objects.equals(todoToTags, tag.todoToTags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tagId, tagName, todoToTags);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                ", todoToTags=" + todoToTags +
                '}';
    }
}
