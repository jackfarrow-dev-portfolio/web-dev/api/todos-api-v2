package com.todosapi.tag;

import com.todosapi.exception.HttpResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
@RequestMapping("api/v2/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("all")
    public ResponseEntity<HttpResponseObject> getAllTags() {
        HttpResponseObject tagsResponse = this.tagService.getAllTags();
        return new ResponseEntity<>(tagsResponse, tagsResponse.getHttpStatus());
    }

    @GetMapping("tag/{id}")
    public ResponseEntity<HttpResponseObject> getTagByTagId(@PathVariable("id") String tagId) {
        HttpResponseObject tagResponse = this.tagService.getTagByTagId(tagId);
        return new ResponseEntity<>(tagResponse, tagResponse.getHttpStatus());
    }

    @PostMapping("create-one")
    public ResponseEntity<HttpResponseObject> createTag(@RequestBody Tag tag) {
        HttpResponseObject tagResponse = this.tagService.createNewTag(tag);
        return new ResponseEntity<>(tagResponse, tagResponse.getHttpStatus());
    }

    @PostMapping("create-all")
    public ResponseEntity<HttpResponseObject> createAllTags(@RequestBody List<Tag> tags) {
        HttpResponseObject tagsResponse = this.tagService.createAllTags(tags);
        return new ResponseEntity<>(tagsResponse, tagsResponse.getHttpStatus());
    }
}
