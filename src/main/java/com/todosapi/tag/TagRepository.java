package com.todosapi.tag;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TagRepository extends CrudRepository<Tag, UUID> {
    Optional<Tag> findByTagName(String name);
}
