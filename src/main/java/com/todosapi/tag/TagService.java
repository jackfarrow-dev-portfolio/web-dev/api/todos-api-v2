package com.todosapi.tag;

import com.todosapi.exception.BadRequestException;
import com.todosapi.exception.HttpResponseObject;
import com.todosapi.exception.ServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public HttpResponseObject getAllTags() {
        try {
            Iterable<Tag> tagsResponse = this.tagRepository.findAll();
            return new HttpResponseObject(HttpStatus.OK, tagsResponse);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }
    public HttpResponseObject getTagByTagId(String tagId) {
        try {
            Optional<Tag> optionalTag = this.tagRepository.findById(UUID.fromString(tagId));
            if (optionalTag.isEmpty()) {
                throw new BadRequestException("Tag identified by " + tagId + " not found");
            }
            return new HttpResponseObject(HttpStatus.OK, optionalTag.get());
        }
        catch (IllegalArgumentException e) {
            throw new BadRequestException("Tag identified by " + tagId + " not found");
        }
        catch (Exception e) {
            throw new ServerException("Unable to fetch tag: " + tagId);
        }
    }

    public HttpResponseObject createNewTag(Tag tag) {
        try {
            Optional<Tag> existingTag = this.tagRepository.findByTagName(tag.getTagName());
            if (existingTag.isPresent()) {
                throw new BadRequestException("Tag " + tag.getTagName() + " already exists");
            }
            Tag createdTag = this.tagRepository.save(tag);
            return new HttpResponseObject(HttpStatus.CREATED, createdTag);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

    public HttpResponseObject createAllTags(List<Tag> tags) {
        Iterable<Tag> savedTags = this.tagRepository.findAll();
        ArrayList<String> tagNames = new ArrayList<>();
        savedTags.forEach(t -> {
            tagNames.add(t.getTagName());
        });
        try {
            List<Tag> unsavedTags = new ArrayList<>();
            tags.forEach(t -> {
                if (!tagNames.contains(t.getTagName())) {
                    unsavedTags.add(t);
                }
            });
            Iterable<Tag> newTags = this.tagRepository.saveAll(unsavedTags);
            if (!newTags.iterator().hasNext()) {
                return new HttpResponseObject(HttpStatus.ACCEPTED, newTags);
            }
            return new HttpResponseObject(HttpStatus.CREATED, newTags);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }
}
