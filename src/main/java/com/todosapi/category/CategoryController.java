package com.todosapi.category;

import com.todosapi.exception.HttpResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;

@RestController
@RequestMapping("api/v2/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("all")
    public ResponseEntity<HttpResponseObject> getAllCategories() {
        HttpResponseObject categories = this.categoryService.getAllCategories();
        return new ResponseEntity<>(categories, categories.getHttpStatus());
    }
    @GetMapping("name/{name}")
    public ResponseEntity<HttpResponseObject> getCategoryByCategoryName(@PathVariable String name) {
        HttpResponseObject category = this.categoryService.getCategoryByName(name);
        return new ResponseEntity<>(category, category.getHttpStatus());
    }

    @GetMapping("id/{id}")
    public ResponseEntity<HttpResponseObject> getCategoryByCategoryId(@PathVariable String id) {
        HttpResponseObject category = this.categoryService.getCategoryById(id);
        return new ResponseEntity<>(category, category.getHttpStatus());
    }
}
