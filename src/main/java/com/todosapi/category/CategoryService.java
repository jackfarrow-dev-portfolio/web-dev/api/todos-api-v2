package com.todosapi.category;

import com.todosapi.exception.BadRequestException;
import com.todosapi.exception.HttpResponseObject;
import com.todosapi.exception.NotFoundException;
import com.todosapi.exception.ServerException;
import com.todosapi.http.CategoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public HttpResponseObject getAllCategories() {
        try {
            Iterable<Category> categories = this.categoryRepository.findAll();
            CategoryResponse response = new CategoryResponse(categories);
            return new HttpResponseObject(HttpStatus.OK, response);
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

    public HttpResponseObject getCategoryByName(String name) {
        try {
            Optional<Category> optional = this.categoryRepository.findByCategoryName(name);
            if (optional.isEmpty()) {
                throw new NotFoundException("Category with name " + name + " not found");
            }
            Category category = optional.get();
            return new HttpResponseObject(HttpStatus.OK, category);
        } catch(Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

    public HttpResponseObject getCategoryById(String id) {
        try {
            UUID idAsUUID = UUID.fromString(id);
            Optional<Category> optional = this.categoryRepository.findById(idAsUUID);
            if (optional.isEmpty()) {
                throw new NotFoundException("Category identified by " + id + " not found");
            }
            Category category = optional.get();
            return new HttpResponseObject(HttpStatus.OK, category);
        }
        catch(IllegalArgumentException e) {
            throw new BadRequestException("Value " + id + " is not a valid category id");
        }
        catch(Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

}
