package com.todosapi.todo_to_tag;

import com.todosapi.tag.Tag;
import com.todosapi.todo.Todo;
import jakarta.persistence.*;

import java.util.Objects;

@Entity(name = "TodoToTag")
@Table(name = "todo_to_tag")
public class TodoToTag {

    @EmbeddedId
    private TodoToTagId id;

    @ManyToOne
    @MapsId("todoId")
    @JoinColumn(name = "todo_id")
    private Todo todo;

    @ManyToOne
    @MapsId("tagId")
    @JoinColumn(name="tag_id")
    private Tag tag;

    public TodoToTag() {
    }

    public TodoToTag(Todo todo, Tag tag) {
        this.todo = todo;
        this.tag = tag;
    }

    public TodoToTagId getId() {
        return id;
    }

    public void setId(TodoToTagId id) {
        this.id = id;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoToTag todoToTag = (TodoToTag) o;
        return Objects.equals(id, todoToTag.id) && Objects.equals(todo, todoToTag.todo) && Objects.equals(tag, todoToTag.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, todo, tag);
    }

    @Override
    public String toString() {
        return "TodoToTag{" +
                "id=" + id +
                ", todo=" + todo +
                ", tag=" + tag +
                '}';
    }
}
