package com.todosapi.todo_to_tag;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class TodoToTagId implements Serializable {

    @Column(name="todo_id")
    private UUID todoId;

    @Column(name = "tag_id")
    private UUID tagId;

    public TodoToTagId() {
    }

    public TodoToTagId(UUID todoId, UUID tagId) {
        this.todoId = todoId;
        this.tagId = tagId;
    }

    public UUID getTodoId() {
        return todoId;
    }

    public void setTodoId(UUID todoId) {
        this.todoId = todoId;
    }

    public UUID getTagId() {
        return tagId;
    }

    public void setTagId(UUID tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoToTagId that = (TodoToTagId) o;
        return Objects.equals(todoId, that.todoId) && Objects.equals(tagId, that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(todoId, tagId);
    }

    @Override
    public String toString() {
        return "TodoToTagId{" +
                "todoId=" + todoId +
                ", tagId=" + tagId +
                '}';
    }
}
