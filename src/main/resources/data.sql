CREATE TABLE IF NOT EXISTS todo (
    todo_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    title VARCHAR(125) NOT NULL,
    description VARCHAR(500) NOT NULL,
    create_date TIMESTAMP DEFAULT NOW() NOT NULL,
    status VARCHAR(25) NOT NULL
);

ALTER TABLE todo ADD COLUMN IF NOT EXISTS category VARCHAR (50) NOT NULL;

CREATE TABLE IF NOT EXISTS tag (
    tag_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    tag_name VARCHAR (75) NOT NULL
);

CREATE TABLE IF NOT EXISTS category (
    category_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    category_name VARCHAR (75) NOT NULL
);


CREATE TABLE IF NOT EXISTS todo_to_tag (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    tag_id UUID NOT NULL,
    todo_id UUID NOT NULL,
    CONSTRAINT fk_todo
        FOREIGN KEY (todo_id)
        REFERENCES todo(todo_id)
        ON DELETE CASCADE,
    CONSTRAINT fk_tag
        FOREIGN KEY (tag_id)
        REFERENCES tag(tag_id)
        ON DELETE CASCADE
);